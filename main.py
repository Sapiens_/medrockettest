import os
import json
import time
import datetime

import requests


def chipping_string(string):
    if len(string) > 46:
        return string[0:46] + "..."
    else:
        return string


def rename_file_and_get_new_path(path):
    try:
        # Проверка на существование текстового файла
        # Если файл уже есть, то переименовываем его по правилам
        if os.path.exists(path):
            time_created = os.path.getmtime(path)
            date_create = datetime.datetime.fromtimestamp(time_created)
            date_string = str(date_create.year) + "-" + str(date_create.month) + "-" + str(date_create.day) + "T" + str(
                date_create.hour) + ":" + str(date_create.minute)
            new_path = 'tasks/' + 'old_' + user["username"] + '_' + date_string + '.txt'
            os.rename(path, new_path)
            return new_path
    except BaseException:
        print("Проблема переименовывания файла")


usersURL = 'https://json.medrocket.ru/users'
todosURL = 'https://json.medrocket.ru/todos'

# Проверка на существование папки и её создание
if not os.path.isdir("tasks"):
    os.mkdir("tasks")

try:
    users_response = requests.get(usersURL)
    todos_response = requests.get(todosURL)

    users_json = json.loads(users_response.text)
    todos_json = json.loads(todos_response.text)
    for user in users_json:
        count_completed_tasks = 0
        count_not_completed_tasks = 0

        completed_tasks = []
        not_completed_tasks = []

        for todo in todos_json:
            try:
                if user["id"] == todo["userId"]:
                    if todo["completed"]:
                        count_completed_tasks += 1
                        completed_tasks.append(todo["title"])
                    else:
                        count_not_completed_tasks += 1
                        not_completed_tasks.append(todo["title"])
            except BaseException:
                print("Не нашли userId в списке todo")

        path = r"tasks/" + user["username"] + ".txt"
        new_path = rename_file_and_get_new_path(path)

        # Пробуем создать файл
        try:
            user_file = open(path, "w")

            company = user["company"]
            user_file.write("# Отчёт для " + company["name"] + '.' + "\n")

            now = datetime.datetime.now()

            name = user["name"] + " <" + user["email"] + "> "
            date = str(now.day) + "-" + str(now.month) + "-" + str(now.year) + " "
            time = str(now.hour) + ":" + str(now.minute)

            user_file.write(name + date + time + "\n")

            count_tasks = count_completed_tasks + count_not_completed_tasks
            user_file.write("Всего задач: " + str(count_tasks) + "\n")
            user_file.write("\n")
            user_file.write("## Актуальные задачи (" + str(count_not_completed_tasks) + "):" + "\n")
            for task in not_completed_tasks:
                user_file.write("- " + str(chipping_string(task)) + "\n")
            user_file.write("\n")
            user_file.write("## Завершённые задачи (" + str(count_completed_tasks) + "):" + "\n")
            for task in completed_tasks:
                user_file.write("- " + str(chipping_string(task)) + "\n")

            user_file.close()
        except BaseException:
            # Если не получилось корректно создать файл, то удаляем его
            # И переименовываем старый файл
            os.remove(path)
            os.rename(new_path, path)
            print("Не получилось создать файл отчёта..")

except BaseException:
    print('Возникли временные шоколадки..')
